## Daily Report (2023/07/18)

- O: Learned RESTful style,  Pair Programming and SpringBoot
- R: Benefit a lot.
- I:  I have learned about RESTful style. Although I have used RESTful style before, my usage is not very standardized. I did not use negative numbers for resources, and the naming of URL paths is also not standardized. Paired Programming can promote knowledge sharing and help beginners quickly master a certain knowledge. In the process of code development, two developers need to work closely together, understand and support each other, thereby establishing a better team atmosphere.
- D: More standardized use of RESTful style, When encountering techniques that I don't know, I can use paired programming with excellent partner