package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entities.Company;
import com.thoughtworks.springbootemployee.entities.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyRepository companyRepository = new CompanyRepository();
    private final EmployeeRepository employeeRepository = new EmployeeRepository();

    @PostMapping()
    public Company addCompany(@RequestBody Company company) {
        return companyRepository.addCompany(company);
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable("id") Long id) {
        return companyRepository.getCompanyById(id);
    }

    @GetMapping("")
    public List<Company> getCompany(@RequestParam(value = "page", required = false) Integer page,
                                    @RequestParam(value = "size", required = false) Integer size) {
        return companyRepository.getCompanyList(page, size);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Company updateCompany(@PathVariable("id") Long id, @RequestBody Company company) {
        return companyRepository.updateCompany(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable("id") Long id){
        companyRepository.deleteCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeeListByCompanyId(@PathVariable("id") Long id){
        return employeeRepository.getEmployeeListByCompanyId(id);
    }
}
