package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entities.Company;
import com.thoughtworks.springbootemployee.entities.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class CompanyRepository {
    private static final List<Company> companyList = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);
    private static final Integer DEFAULT_PAGE = 1;
    private static final Integer DEFAULT_SIZE = 10;

    public Company addCompany(Company company) {
        company.setId(atomicId.incrementAndGet());
        companyList.add(company);
        return company;
    }

    public Company getCompanyById(Long id) {
        for (Company company : companyList) {
            if (Objects.equals(company.getId(), id)) {
                return company;
            }
        }
        return null;
    }

    public List<Company> getCompanyList(Integer page, Integer size) {
        page = (page != null) ? page : DEFAULT_PAGE;
        size = (size != null) ? size : DEFAULT_SIZE;
        return companyList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company updateCompany(Long id, Company updateCompany) {
        for (Company company : companyList) {
            if (Objects.equals(id, company.getId())) {
                company.setName(updateCompany.getName());
                return company;
            }
        }
        return null;
    }

    public void deleteCompanyById(Long id) {
        for (Company company : companyList) {
            if (Objects.equals(id, company.getId())) {
                companyList.remove(company);
            }
        }
    }
}
