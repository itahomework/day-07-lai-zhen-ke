package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entities.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class EmployeeRepository {

    private static final List<Employee> employeeList = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);
    private static final Integer DEFAULT_PAGE = 1;
    private static final Integer DEFAULT_SIZE = 10;

    public List<Employee> getEmployeeList(String gender, Integer page, Integer size) {
        page = (page != null) ? page : DEFAULT_PAGE;
        size = (size != null) ? size : DEFAULT_SIZE;

        List<Employee> resultList = employeeList.stream()
                .filter(employee -> gender == null || Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());

        resultList = resultList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
        return resultList;
    }

    public Employee addEmployee(Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employeeList.add(employee);
        return employee;
    }

    public Employee getEmployeeById(Long id) {
        for (Employee employee : employeeList) {
            if (Objects.equals(employee.getId(), id)) {
                return employee;
            }
        }
        return null;
    }

    public Employee updateEmployee(Long id, Employee updateEmployee) {
        for (Employee employee : employeeList) {
            if (Objects.equals(id, employee.getId())) {
                employee.setAge(updateEmployee.getAge());
                employee.setSalary(updateEmployee.getSalary());
                return employee;
            }
        }
        return null;
    }
    public void deleteEmployeeById(Long id) {
        employeeList.removeIf(employee -> Objects.equals(id, employee.getId()));
    }

    public List<Employee> getEmployeeListByCompanyId(Long companyId){
        return employeeList.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }
}
